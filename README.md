# `Ma_124/pages-static-shared`
This repository contains static files for various READMEs and pages by Ma_124.

## Awesome (`awesome-*`)
Logo and Badge for [Awesome Framework](https://gitlab.com/Ma_124/awesome-framework).

Uses:
- [Awesome Framework/README.md](https://gitlab.com/Ma_124/awesome-framework)

## Discord Screenshots (`discord_devmode.png`)
Shows how to enable Developer Mode in Discord.

Uses:
- [`libdcbot`/README.md](https://gitlab.com/Ma_124/libdcbot)

## Gin Auth (`ginauth2.svg`)
Logo for [Gin Auth](https://gitlab.com/Ma_124/ginauth).

Uses:
- [Gin Auth/README.md](https://gitlab.com/Ma_124/ginauth)

Credits:
- [Gin-Gonic Framework Logo](https://github.com/gin-gonic/logo#license) by Javier Provecho licensed under [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)
- Key Icon by [Freepik](https://www.freepik.com/home) from www.flaticon.com

## Gin Log (`ginlog-screenshot1.png`)
Example log output from [Gin Log](https://gitlab.com/Ma_124/ginlog).

Uses:
- [Gin Log/README.md](https://gitlab.com/Ma_124/ginlog)

## KDE Screenshots (`kde_*`)
Tutorial for enabling [DynWall 1 & 2](https://gitlab.com/Ma_124/dynwall2).

Uses:
- [Dynwall 2/README.md](https://gitlab.com/Ma_124/dynwall2)
- [Dynwall/README.md](https://gitlab.com/Ma_124/dynwall)

## NYAND (`nand_*`)
Example images for [nyand](https://gitlab.com/Ma_124/nyand).

Uses:
- [nyand/README.md](https://gitlab.com/Ma_124/nyand)

Credits:
- Nyan Cat was originally drawn by Christopher Torres as "Pop-Tart Cat"
- NYAND Cat was originally drawn by Matt Parker based on Nyan Cat.

## Vim Screenshots (`vim`)
Old screenshots of my VIM.
